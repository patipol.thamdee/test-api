package main

import (
	"fmt"
	"myapi/config"
	"myapi/routes"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

var err error

func main() {

	config.DB, err = gorm.Open("mysql", config.DbURL(config.BuildDBConfig()))
	if err != nil {
		fmt.Println("statuse: ", err)
	}
	config.DB.LogMode(true)

	defer config.DB.Close()

	r := gin.Default()

	// r.GET("/ping", func(c *gin.Context) {
	// 	c.JSON(200, gin.H{
	// 		"message": "pong",
	// 	})
	// })
	// config.DB.AutoMigrate(&model.Actor{})
	routes.SetupRouter(r)
	r.Run(":5001")
}
