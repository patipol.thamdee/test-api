package routes

import (
	"myapi/controller"
	"myapi/middleware"

	"github.com/gin-gonic/gin"
)

func SetupRouter(r *gin.Engine) *gin.Engine {

	r.Use(gin.Recovery())

	v1 := r.Group("/v1")
	{
		v1.Use(middleware.ResponseWraper())
		booking := v1.Group("/bookings")
		booking.GET("/", controller.GetBookings)
		booking.GET("/report", controller.GetBookingReport)
		booking.GET("/report/income", controller.GetIncomeReport)
	}

	return r
}
