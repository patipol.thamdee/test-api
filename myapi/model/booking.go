package model

import (
	"myapi/config"
	"strconv"
	"strings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// func paginate() func(config.DB *gorm.DB) *gorm.DB {

// 	return func(config.DB *gorm.DB) *gorm.DB {
// 		return config.DB.Offset(0).Limit(2)
// 	}

// }

func paginate(queryParams map[string][]string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		var page = 1
		if val, ok := queryParams["page"]; ok {
			var err error
			page, err = strconv.Atoi(val[0])
			if err != nil {
				page = 1
			}
		}
		var pageSize = 10
		offset := (page - 1) * pageSize
		return db.Offset(offset).Limit(pageSize)
	}
}

func bookingCond(queryParams map[string][]string) (interface{}, []interface{}) {
	var query string
	var bindings []interface{}
	query += " TRUE "

	if val, ok := queryParams["name"]; ok {
		query += " AND CONCAT(c.firstname,' ',c.lastname) LIKE '%" + val[0] + "%'"
		// bindings = append(bindings, val[0])
	}

	if val, ok := queryParams["age"]; ok {
		if val[0] == ">51" {
			query += " AND TIMESTAMPDIFF(YEAR, c.birthday, CURDATE()) > 51"
		} else {
			d := strings.Split(val[0], "-")
			query += " AND TIMESTAMPDIFF(YEAR, c.birthday, CURDATE()) BETWEEN " + d[0] + " AND " + d[1]
		}
		// query += " AND CONCAT(customers.firstname,' ',customers.lastname) = ?"
		// bindings = append(bindings, val[0])

	}

	return query, bindings
}

func GetBookings(customerBookings *[]CustomerBookingRoom, queryParams map[string][]string) (err error) {
	if err := config.DB.Scopes(paginate(queryParams)).Table("bookings b").Select(`
	c.firstname,
	c.lastname,
	c.mobile,
	c.email,
	b.checkin_date,
	b.checkout_date,
	r.name AS 'room_name',
	r.room_rate,
	r.type
	`).Joins(`
	LEFT JOIN customers c ON 
	c.id = b.customer_id
	`).Joins(`
	LEFT JOIN rooms r on 
	r.id = b.room_id
	`).Where(bookingCond(queryParams)).Order("b.id desc").Find(&customerBookings).Error; err != nil {
		return err
	}
	return nil
}

func bookingReportDaily() (interface{}, error) {
	var m []ReportBookingByDate
	if err := config.DB.Table("bookings").Select(`
	COUNT(1) AS 'booking_amount', checkin_date AS 'booking_date'
	`).Group("checkin_date").Find(&m).Error; err != nil {
		return nil, err
	}
	return m, nil
}

func bookingReportMonthly() (interface{}, error) {
	var m []ReportBookingByMonth
	if err := config.DB.Table("bookings").Select(`
	COUNT(1) AS 'booking_amount', DATE_FORMAT(checkin_date,"%Y-%m") AS 'booking_date'
	`).Group(`DATE_FORMAT(checkin_date,"%Y-%m")`).Find(&m).Error; err != nil {
		return nil, err
	}
	return m, nil
}

func bookingReportQuarterly() (interface{}, error) {
	var m []ReportBookingByQuarter
	if err := config.DB.Table("bookings").Select(`
	COUNT(1) AS 'booking_amount', QUARTER(checkin_date) AS 'booking_date'
	`).Group(`QUARTER(checkin_date)`).Find(&m).Error; err != nil {
		return nil, err
	}
	return m, nil
}

func GetBookingReport(queryParams map[string][]string) (interface{}, error) {

	var bookingType string
	if _, ok := queryParams["type"]; !ok {
		bookingType = "daily"
	} else {
		bookingType = queryParams["type"][0]
	}

	switch bookingType {
	case "daily":
		return bookingReportDaily()
	case "monthly":
		return bookingReportMonthly()
	case "quarterly":
		return bookingReportQuarterly()
	default:
		return bookingReportDaily()
	}
}

func incomeReportMonthly() (interface{}, error) {
	var m []ReportIncomeByMonthly
	if err := config.DB.Table("rooms r").Select(`
	SUM(r.room_rate) AS 'income',  DATE_FORMAT(b.checkin_date,"%Y-%m")) AS 'booking_date'
	`).Joins(`
	INNER JOIN bookings b
	ON b.room_id = r.id`).Group(`DATE_FORMAT(b.checkin_date,"%Y-%m")`).Find(&m).Error; err != nil {
		return nil, err
	}
	return m, nil
}

func incomeReportQuarterly() (interface{}, error) {
	var m []ReportIncomeByQuarterly
	if err := config.DB.Table("rooms r").Select(`
	SUM(r.room_rate) AS 'income',  QUARTER(b.checkin_date) AS 'booking_date'
	`).Joins(`
	INNER JOIN bookings b
	ON b.room_id = r.id`).Group(`QUARTER(b.checkin_date)`).Find(&m).Error; err != nil {
		return nil, err
	}
	return m, nil
}

func GetIncomeReport(queryParams map[string][]string) (interface{}, error) {

	var bookingType string
	if _, ok := queryParams["type"]; !ok {
		bookingType = "monthly"
	} else {
		bookingType = queryParams["type"][0]
	}

	switch bookingType {

	case "monthly":
		return incomeReportMonthly()
	case "quarterly":
		return incomeReportQuarterly()
	default:
		return incomeReportMonthly()

	}
}

// func GetReport
