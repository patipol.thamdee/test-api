package model

import "gopkg.in/guregu/null.v4"

type Booking struct {
	ID           int64       `json:"id" gorm:"id"`
	RoomID       null.Int    `json:"roomID" gorm:"column:room_id"`
	CustomerID   null.Int    `json:"customerID" gorm:"column:customer_id"`
	CheckinDate  null.String `json:"checkinDate" gorm:"column:checkin_date"`
	CheckoutDate null.String `json:"checkoutDate" gorm:"column:checkout_date"`
}

type Customer struct {
	ID        int64       `json:"id" gorm:"id"`
	FirstName null.String `json:"firstName" gorm:"column:firstname"`
	LastName  null.String `json:"lastName" gorm:"column:lastname"`
	Mobile    null.String `json:"mobile" gorm:"column:mobile"`
	Email     null.String `json:"email" gorm:"column:email"`
	Gender    null.String `json:"gender" gorm:"column:gender"`
	Birthday  null.String `json:"birthday" gorm:"column:birthday"`
}

type Room struct {
	ID       int64       `json:"id" gorm:"id"`
	Name     null.String `json:"name" gorm:"column:name"`
	RoomRate null.Float  `json:"roomRate" gorm:"column:room_rate"`
	Type     null.String `json:"type" gorm:"column:type"`
}

type CustomerBookingRoom struct {
	FirstName    null.String `json:"firstName" gorm:"column:firstname"`
	LastName     null.String `json:"lastName" gorm:"column:lastname"`
	Mobile       null.String `json:"mobile"`
	Email        null.String `json:"email"`
	CheckinDate  null.String `json:"checkinDate"`
	CheckoutDate null.String `json:"checkoutDate"`
	RoomName     null.String `json:"roomName"`
	RoomRate     null.Float  `json:"roomRate"`
	RoomType     null.String `json:"roomType" gorm:"column:type"`
}

type ReportBookingByDate struct {
	Date   null.String `json:"date" gorm:"column:booking_date"`
	Amount null.Int    `json:"amount" gorm:"column:booking_amount"`
}

type ReportBookingByMonth struct {
	Date   null.String `json:"month" gorm:"column:booking_date"`
	Amount null.Int    `json:"amount" gorm:"column:booking_amount"`
}

type ReportBookingByQuarter struct {
	Date   null.String `json:"quarter" gorm:"column:booking_date"`
	Amount null.Int    `json:"amount" gorm:"column:booking_amount"`
}

type ReportIncomeByMonthly struct {
	Date   null.String `json:"month" gorm:"column:booking_date"`
	Amount null.Int    `json:"income" gorm:"column:income"`
}

type ReportIncomeByQuarterly struct {
	Date   null.String `json:"quarter" gorm:"column:booking_date"`
	Amount null.Int    `json:"income" gorm:"column:income"`
}

type Response struct {
	Data         interface{} `json:"data"`
	IntervalTime int64       `json:"intervalTime"`
}

// func (b *Booking) TableName() string {
// 	return "bookings"
// }

// func (b *Booking) TableName() string {
// 	return "actor"
// }

// func (b *Booking) TableName() string {
// 	return "actor"
// }

// func (b *Booking) TableName() string {
// 	return "actor"
// }
