package middleware

import "github.com/gin-gonic/gin"

func ResponseWraper() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer wrap(c)
		c.Next()
	}
}

//TODO
func wrap(c *gin.Context) {

}
