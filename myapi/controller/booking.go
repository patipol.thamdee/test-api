package controller

import (
	"fmt"
	"myapi/model"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func GetBookings(c *gin.Context) {
	queryParams := c.Request.URL.Query()
	fmt.Println(queryParams)
	var customerBookings []model.CustomerBookingRoom
	start := time.Now()
	err := model.GetBookings(&customerBookings, queryParams)
	duration := time.Since(start)

	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, model.Response{customerBookings, duration.Milliseconds()})
	}
}

func GetBookingReport(c *gin.Context) {
	queryParams := c.Request.URL.Query()
	start := time.Now()
	data, err := model.GetBookingReport(queryParams)
	duration := time.Since(start)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, model.Response{data, duration.Milliseconds()})
	}
}

func GetIncomeReport(c *gin.Context) {
	queryParams := c.Request.URL.Query()
	start := time.Now()
	data, err := model.GetBookingReport(queryParams)
	duration := time.Since(start)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, model.Response{data, duration.Milliseconds()})
	}
}
